FROM nginx:1.27.0-alpine3.19-slim

COPY src /usr/share/nginx/html/
COPY default.conf /etc/nginx/conf.d
COPY nginx.conf /etc/nginx/

RUN chgrp -R 0 /etc/nginx  && chmod -R g=u /etc/nginx &&\
    chgrp -R 0 /var/cache/nginx  && chmod -R g=u /var/cache/nginx &&\
    chgrp -R 0 /var/log/nginx  && chmod -R g=u /var/log/nginx
#RUN chgrp -R 0 /var/run && chmod -R g=u /var/run

#EXPOSE 8000
#CMD npm run  build
