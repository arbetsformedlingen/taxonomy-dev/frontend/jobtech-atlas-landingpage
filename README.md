# Jobtech Atlas - Landingpage    
Updates web content on the Jobtech Atlas landing page. 

Web content (informational content in text, pictures, etc.) is specified through the HTML-files in the repository. The files are specified for each new version of the database content ("v1", "v2", "v3" etc).

# Editor - User manual
Edit the user manual for the taxonomy editor.

To add a new entry into the manual simply add `<entry></entry>` tags into the **src/editor/manual.html** file.\
Inside these tags three properties can be set.

1. Set the title that best describes the entry.\
`<title>My custom title</title>`

2. Set one or more tags in a combination to enable quick filtering for the entry.\
Recommendation is to atleast have one.\
Available tags: **create**, **remove**, **settings**, **other**\
`<tags>settings, other</tags>`

3. Set text that explains the entry.\
Inside this tag its possible to use html tags.\
`<text>This text can include <b>html</b> tags</text>`

When all changes are done and the file have been commited. It takes about 2-5 minutes for that changes to become active, then simple refresh the client to see the new entry.
